import React, { Component } from 'react'

class RobotForm extends Component {
    handleSubmit = (event) => {
        event.preventDefault();
        const name = document.getElementById("name").value;
        const type = document.getElementById("type").value;
        const mass = document.getElementById("mass").value;

        if(name.length > 0 && type.length > 0 && mass.length > 0){
            const ceva = {name, type, mass};
            this.props.onAdd(ceva)
        } else {
            console.warn("Eroare")
        }
    }
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit} >
            <input id="name"/>
            <input id="type"/>
            <input id="mass"/>
            <button type="submit" id="add" value="add">add</button>
        </form>
      </div>
    )
  }
}

export default RobotForm